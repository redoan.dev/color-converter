<?php
include_once "index_function.php";
    if(isset($_POST["key"]) && $_POST["key"] !=''){
        $key=$_POST["key"];
        }else{
            $_POST["key"]="BurlyWood";
        }
$names=['AliceBlue','AntiqueWhite','Aqua','Aquamarine','Azure','Beige','Bisque','Black','BlanchedAlmond','Blue','BlueViolet','Brown','BurlyWood','CadetBlue','Chartreuse','Chocolate','Coral','CornflowerBlue','Cornsilk','Crimson','Cyan','DarkBlue','DarkCyan','DarkGoldenRod','DarkGray','DarkGrey','DarkGreen','DarkKhaki','DarkMagenta','DarkOliveGreen','DarkOrange','DarkOrchid','DarkRed','DarkSalmon','DarkSeaGreen','DarkSlateBlue','DarkSlateGray','DarkSlateGrey','DarkTurquoise','DarkViolet','DeepPink','DeepSkyBlue','DimGray','DimGrey','DodgerBlue','FireBrick','FloralWhite','ForestGreen','Fuchsia','Gainsboro','GhostWhite','Gold','GoldenRod','Gray','Grey','Green','GreenYellow','HoneyDew','HotPink','IndianRed','Indigo','Ivory','Khaki','Lavender','LavenderBlush','LawnGreen','LemonChiffon','LightBlue','LightCoral','LightCyan','LightGoldenRodYellow','LightGray','LightGrey','LightGreen','LightPink','LightSalmon','LightSeaGreen','LightSkyBlue','LightSlateGray','LightSlateGrey','LightSteelBlue','LightYellow','Lime','LimeGreen','Linen','Magenta','Maroon','MediumAquaMarine','MediumBlue','MediumOrchid','MediumPurple','MediumSeaGreen','MediumSlateBlue','MediumSpringGreen','MediumTurquoise','MediumVioletRed','MidnightBlue','MintCream','MistyRose','Moccasin','NavajoWhite','Navy','OldLace','Olive','OliveDrab','Orange','OrangeRed','Orchid','PaleGoldenRod','PaleGreen','PaleTurquoise','PaleVioletRed','PapayaWhip','PeachPuff','Peru','Pink','Plum','PowderBlue','Purple','RebeccaPurple','Red','RosyBrown','RoyalBlue','SaddleBrown','Salmon','SandyBrown','SeaGreen','SeaShell','Sienna','Silver','SkyBlue','SlateBlue','SlateGray','SlateGrey','Snow','SpringGreen','SteelBlue','Tan','Teal','Thistle','Tomato','Turquoise','Violet','Wheat','White','WhiteSmoke','Yellow','YellowGreen'];
$hexValue=['f0f8ff','faebd7','00ffff','7fffd4','f0ffff','f5f5dc','ffe4c4','000000','ffebcd','0000ff','8a2be2','a52a2a','deb887','5f9ea0','7fff00','d2691e','ff7f50','6495ed','fff8dc','dc143c','00ffff','00008b','008b8b','b8860b','a9a9a9','a9a9a9','006400','bdb76b','8b008b','556b2f','ff8c00','9932cc','8b0000','e9967a','8fbc8f','483d8b','2f4f4f','2f4f4f','00ced1','9400d3','ff1493','00bfff','696969','696969','1e90ff','b22222','fffaf0','228b22','ff00ff','dcdcdc','f8f8ff','ffd700','daa520','808080','808080','008000','adff2f','f0fff0','ff69b4','cd5c5c','4b0082','fffff0','f0e68c','e6e6fa','fff0f5','7cfc00','fffacd','add8e6','f08080','e0ffff','fafad2','d3d3d3','d3d3d3','90ee90','ffb6c1','ffa07a','20b2aa','87cefa','778899','778899','b0c4de','ffffe0','00ff00','32cd32','faf0e6','ff00ff','800000','66cdaa','0000cd','ba55d3','9370db','3cb371','7b68ee','00fa9a','48d1cc','c71585','191970','f5fffa','ffe4e1','ffe4b5','ffdead','000080','fdf5e6','808000','6b8e23','ffa500','ff4500','da70d6','eee8aa','98fb98','afeeee','db7093','ffefd5','ffdab9','cd853f','ffc0cb','dda0dd','b0e0e6','800080','663399','ff0000','bc8f8f','4169e1','8b4513','fa8072','f4a460','2e8b57','fff5ee','a0522d','c0c0c0','87ceeb','6a5acd','708090','708090','fffafa','00ff7f','4682b4','d2b48c','008080','d8bfd8','ff6347','40e0d0','ee82ee','f5deb3','ffffff','f5f5f5','ffff00','9acd32'];
for($i=0;$i<count($hexValue);$i++){
    list($r,$g,$b)=sscanf($hexValue[$i],"%2x%2x%2x");
    $rgbValue[$i]="$r,$g,$b";
    $rgbV[$i]="rgb($r,$g,$b)";
    $hexV[$i]="#$hexValue[$i]";
    //rgb to hsl
    $R=$r/255;
    $G=$g/255;
    $B=$b/255;
    $min=min($R,$G,$B);
    $max=max($R,$G,$B);
    $l=(($max+$min)/2);
    $roundL=round($l*100);
    if(($max-$min) == 0){
        $s=0;
    }else{
        $s=($max-$min) / (1 - abs(2 * $l - 1));
    }
    $roundS=round($s*100);
    if($max==$min){
        $roundH=0;
    }elseif($max==$R){
        $h=(($G-$B)/($max-$min))*60;
        $roundH=round($h);
    }elseif($max==$G){
        $h=(2.0+(($B-$R)/($max-$min)))*60;
        $roundH=round($h);
    }elseif($max==$B){
        $h=(4.0+(($R-$G)/($max-$min)))*60;
        $roundH=round($h);
    }
    if ($roundH < 0) {
        $roundH = $roundH + 360;}
    $hslV[$i]="hsl($roundH,$roundS%,$roundL%)";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>COLOR CONVERTER</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.min.css">
        <style>
            .colorName {
                text-transform: capitalize;
                background-color: darkseagreen;
                color: black;
                font-weight: bold;
                font-size: 25px;
                border: 2px solid deepskyblue;
                padding: 5px;
            }
        </style>
    </head>
    <body>
    <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <h1>COLOR CONVERTER</h1>
                    <p>Use this color in our Color Picker</p>
                </div>
            </div>
            <div class="row">
    
                <div class="column">
                    <form method="POST" action="index.php">
                        <label style="color:dimgray;"><em>Name,Hex,RGB,HSL</em></label>
                        <input type="text" name="key" id="key" value="<?php echo name();?>" >
                        <label style="color:dimgray;"><em>COLOR VIEW</em></label>
                        <table>
                            <tr>
                                <td>Name</td>
                                <td><?php echo colorName();?></td>
                            </tr>
                            <tr>
                                <td>HEX</td>
                                <td><?php echo colorHex();?></td>
                            </tr>
                            <tr>
                                <td>RGB</td>
                                <td><?php echo colorRgb();?></td>
                            </tr>
                            <tr>
                                <td>HSL</td>
                                <td><?php echo colorHsl();?></td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="column">
                        <div style="background-color:<?php echo name();?>;text-align:center; border: 5px double darkgray; padding: 84px;margin-top: 120px;"><span class="colorName"><?php echo name();?></span></div>
                </div>
            
        </div>
    </div>
</body>
</html>